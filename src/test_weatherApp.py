"""
Test module for the weatherApp module.

This module contains unit tests for the weatherApp module functions.
"""

import unittest
from weatherApp import get_current_weather, get_forecast, kelvin_to_celsius, get_average_temperature


class TestWeatherApp(unittest.TestCase):
    """Unittest test class"""

    def test_get_current_weather(self):
        """Test the get_current_weather function"""
        weather_data = get_current_weather("Stockholm")
        self.assertIsNotNone(weather_data)

    def test_get_forecast(self):
        """Test the get_forecast function"""
        forecast_data = get_forecast("Stockholm", "daily")
        self.assertIsNotNone(forecast_data)

        forecast_data = get_forecast("Stockholm", "weekly")
        self.assertIsNotNone(forecast_data)

    def test_kelvin_to_celsius(self):
        """Test the kelvin_to_celsius function"""
        celsius_temp = kelvin_to_celsius(300)
        self.assertAlmostEqual(celsius_temp, 26.85, places=2)

    def test_get_average_temperature(self):
        """Test the get_average_temperature function"""
        average_temp = get_average_temperature("Stockholm")
        self.assertIsNotNone(average_temp)


if __name__ == '__main__':
    unittest.main()
