"""
Weather App

This module provides functionality to retrieve and display weather information using the OpenWeatherMap API.
"""

import datetime
import argparse
import requests

API_KEY = "7f50f728004e7013f94e02b213f5c3de"


def get_current_weather(city):
    """
    Get the current weather data for a given city.

    Args:
        city (str): The name of the city.

    Returns:
        dict: The JSON response containing the weather data, or None if the request fails.
    """
    url = (
        f"https://api.openweathermap.org/data/2.5/weather?q={city}"
        f"&appid={API_KEY}"
    )
    response = requests.get(url, timeout=5)
    if response.status_code == 200:
        return response.json()
    return None


def get_forecast(city, forecast_type, date=None):
    """
    Get the forecast data for a given city, forecast type, and date.

    Args:
        city (str): The name of the city.
        forecast_type (str): The type of forecast (daily or weekly).
        date (str): The specific date in the format "YYYY-MM-DD" (optional).

    Returns:
        dict: The JSON response containing the forecast data, or None if the request fails.
    """
    url = (
        f"https://api.openweathermap.org/data/2.5/forecast?q={city}"
        f"&appid={API_KEY}"
    )
    response = requests.get(url, timeout=5)
    if response.status_code == 200:
        forecast_data = response.json()
        if date:
            forecast = [forecast for forecast in forecast_data["list"] if forecast["dt_txt"].startswith(date)]
            return forecast
        if forecast_type == "daily":
            today = datetime.date.today().strftime("%Y-%m-%d")
            daily_forecast = [forecast for forecast in forecast_data["list"] if forecast["dt_txt"].startswith(today)]
            return daily_forecast
        return forecast_data["list"]
    return None


def kelvin_to_celsius(kelvin):
    """
    Convert temperature from Kelvin to Celsius.

    Args:
        kelvin (float): Temperature in Kelvin.

    Returns:
        float: Temperature in Celsius.
    """
    return kelvin - 273.15


def get_average_temperature(city):
    """
    Get the average temperature for the next 7 days in a given city.

    Args:
        city (str): The name of the city.

    Returns:
        float: The average temperature in Celsius, or None if the request fails.
    """
    forecast_data = get_forecast(city, "daily")
    if forecast_data:
        temperatures = [kelvin_to_celsius(day["main"]["temp"]) for day in forecast_data]
        average_temperature = sum(temperatures) / len(temperatures)
        return average_temperature
    return None


def get_weather_info(city, option, date=None):
    """
    Get weather information for a given city, option, and date.

    Args:
        city (str): The name of the city.
        option (str): The weather option (current, daily, weekly, average).
        date (str): The specific date in the format "YYYY-MM-DD" (optional).

    Returns:
        None
    """
    if option == "current":
        weather_data = get_current_weather(city)
        if weather_data:
            temperature = kelvin_to_celsius(weather_data["main"]["temp"])
            description = weather_data["weather"][0]["description"]
            print(f"Current weather in {city}:")
            print(f"  - Temperature: {temperature}°C")
            print(f"  - Weather Description: {description}")
        else:
            print(f"Failed to retrieve current weather for {city}")

    elif option in ["daily", "weekly"]:
        forecast_data = get_forecast(city, option, date)
        if forecast_data:
            forecast_type = option.capitalize()
            print(f"{forecast_type} forecast for {city}:")
            for forecast in forecast_data:
                date = forecast["dt_txt"].split()[0]
                temperature = kelvin_to_celsius(forecast["main"]["temp"])
                description = forecast["weather"][0]["description"]
                print(f"Date: {date}, Temperature: {temperature}°C, Description: {description}")
        else:
            print(f"Failed to retrieve {option} forecast for {city}")

    elif option == "average":
        average_temp = get_average_temperature(city)
        if average_temp:
            print(f"Average temperature for the next 7 days in {city}: {average_temp}°C")
        else:
            print(f"Failed to retrieve average temperature for {city}")

    else:
        print("Invalid option. Available options: current, daily, weekly, average")


def main():
    """
    Entry point of the weather app.
    """
    parser = argparse.ArgumentParser(description='Weather App')
    parser.add_argument('city', type=str, help='City name')
    parser.add_argument('option', type=str, help='Weather option (current, daily, weekly, average)')
    parser.add_argument('date', nargs='?', type=str, help='Specific date in the format "YYYY-MM-DD" (optional)')
    args = parser.parse_args()

    city = args.city.capitalize()
    option = args.option.lower()
    date = args.date

    get_weather_info(city, option, date)


if __name__ == "__main__":
    main()
